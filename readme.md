## Face Control
An android app that uses gaze detection to controll interactions with the app.

**External Dependencies** - openCV - [link](https://sourceforge.net/projects/opencvlibrary/files/opencv-android/)

**Steps to import opencv**

1. Download the openCV android SDK from the above link and extract the contents

2. copy the contents of `/sdk/native/libs/` to `[ProjectName]/app/src/main/jniLibs/`

3. Import and build project in Android Studio
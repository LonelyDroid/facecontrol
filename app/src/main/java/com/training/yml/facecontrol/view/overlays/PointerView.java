package com.training.yml.facecontrol.view.overlays;

import android.graphics.RectF;
import android.util.Pair;


public interface PointerView {

    void addPoint(Pair<Integer, Integer> point);

    void addRect(RectF rectangle);

    void clear();

    void reDraw();

    void setColor(int color);
}

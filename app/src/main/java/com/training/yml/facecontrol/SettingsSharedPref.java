package com.training.yml.facecontrol;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.IntDef;

public class SettingsSharedPref {

    private static final String SHARED_PREF_TAG = "com.training.yml.FACE_CONTROL_SETTINGS";
    private static final String CLASSIFIER_KEY = "com.training.yml.CLASSIFIER";
    private static final String X_SENSITIVITY_KEY = "com.training.yml.X_SENSITIVITY";
    private static final String Y_SENSITIVITY_KEY = "com.training.yml.Y_SENSITIVITY";
    private static final String SHOW_VERTICAL_LIST_KEY = "com.training.yml.SHOW_VERTICAL_LIST";
    private static final String SHOW_HORIZONTAL_LIST_KEY = "com.training.yml.SHOW_HORIZONTAL_LIST";

    public static final short NONE = -1;
    public static final short OPEN_CV = 0;
    public static final short FML_KIT = 1;

    @IntDef({OPEN_CV, FML_KIT})
    public @interface ClassifierType {
    }

    private SharedPreferences preferences;

    public SettingsSharedPref(Context context) {
        preferences = context.getSharedPreferences(SHARED_PREF_TAG, Context.MODE_PRIVATE);
        if (preferences.getInt(CLASSIFIER_KEY, NONE) == NONE) { // write defaults
            setDefaults();
        }
    }

    public void setClassifier(@ClassifierType int classifier) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(CLASSIFIER_KEY, classifier);
        editor.apply();
    }

    public @ClassifierType
    int getClassifier() {
        return preferences.getInt(CLASSIFIER_KEY, NONE);
    }

    public void setXSensitivity(float sensitivity) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(X_SENSITIVITY_KEY, sensitivity);
        editor.apply();
    }

    public float getXSensitivity() {
        return preferences.getFloat(X_SENSITIVITY_KEY, 3);
    }

    public void setYSensitivity(float sensitivity) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(Y_SENSITIVITY_KEY, sensitivity);
        editor.apply();
    }

    public float getYSensitivity() {
        return preferences.getFloat(Y_SENSITIVITY_KEY, 3);
    }

    public void setShowVerticalList(boolean visibility) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SHOW_VERTICAL_LIST_KEY, visibility);
        editor.apply();
    }

    public boolean getShowVerticalList() {
        return preferences.getBoolean(SHOW_VERTICAL_LIST_KEY, false);
    }

    public void setShowHorizontalList(boolean visibility) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SHOW_HORIZONTAL_LIST_KEY, visibility);
        editor.apply();
    }

    public boolean getShowHorizontalList() {
        return preferences.getBoolean(SHOW_HORIZONTAL_LIST_KEY, false);
    }

    public void setDefaults() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(CLASSIFIER_KEY, OPEN_CV);
        editor.putFloat(X_SENSITIVITY_KEY, 3f);
        editor.putFloat(Y_SENSITIVITY_KEY, 3f);
        editor.putBoolean(SHOW_HORIZONTAL_LIST_KEY, false);
        editor.putBoolean(SHOW_VERTICAL_LIST_KEY, false);
        editor.apply();
    }
}

package com.training.yml.facecontrol.classifier;

import android.graphics.Bitmap;
import android.graphics.RectF;
import android.util.Log;

import com.training.yml.facecontrol.view.ControlView;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class OpenCVFaceDetector extends FaceDetector {

    private static final String TAG = OpenCVFaceDetector.class.getName();

    private CascadeClassifier faceClassifier;
    private CascadeClassifier eyeClassifier;
    private CompositeDisposable disposables;

    private volatile boolean processing = false;

    public OpenCVFaceDetector(CascadeClassifier faceClassifier, CascadeClassifier eyeClassifier, ControlView controlView) {
        super(controlView);
        this.faceClassifier = faceClassifier;
        this.eyeClassifier = eyeClassifier;
        disposables = new CompositeDisposable();
    }


    @Override
    public void processBitmap(Bitmap image, boolean detectLandmarks) {
        final RectF rightEye = new RectF();
        final RectF leftEye = new RectF();
        if (!processing) {
            processing = true;
            disposables.clear();
            final Disposable subscribe = Completable.fromAction(() -> {
                Mat inputFrameMat = new Mat(image.getWidth(), image.getHeight(), CvType.CV_8UC3);

                Utils.bitmapToMat(image, inputFrameMat);
                Imgproc.cvtColor(inputFrameMat, inputFrameMat, Imgproc.COLOR_RGBA2RGB);

                Rect[] facesArray = getFaces(inputFrameMat);
                Log.i(TAG, "processBitmap: faces : " + facesArray.length);
                if (facesArray.length == 1) {

                    final RectF faceRectF = getRectF(facesArray[0]);

                    trueFaceCenter[0] = (int) faceRectF.centerX();
                    trueFaceCenter[1] = (int) faceRectF.centerY();

                    if (detectLandmarks) {
                        Mat faceMat = inputFrameMat.submat(facesArray[0]);
                        final Rect[] eyesArray = getEyes(faceMat);
                        Log.i(TAG, "processBitmap: eyes : " + eyesArray.length);
                        if (eyesArray.length == 2) {
                            rightEye.set(getRectF(eyesArray[0], (int) faceRectF.left, (int) faceRectF.top));
                            leftEye.set(getRectF(eyesArray[1], (int) faceRectF.left, (int) faceRectF.top));
                        }
                    }
                }
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        if (pointerView != null) {
                            scale(trueFaceCenter[0], trueFaceCenter[1]);
                            if (detectLandmarks) {
                                if (pointerView != null) {
                                    pointerView.addRect(rightEye);
                                    pointerView.addRect(leftEye);
                                }
                            }
                            pointerView.addRect(new RectF(cursorRect));
                            pointerView.reDraw();
                            pointerView.clear();
                            processing = false;
                        }
                    });

            disposables.add(subscribe);
        }
    }

    /**
     * Uses the HarrCascade classifier to identify a face in an image
     *
     * @param image
     * @return An array of faces detected by the classifier
     */
    private Rect[] getFaces(Mat image) {
        int absoluteFaceSize = (int) (image.height() * 0.2);
        MatOfRect faces = new MatOfRect();
        if (faceClassifier != null) {
            faceClassifier.detectMultiScale(image, faces, 1.1, 2, 2,
                    new Size(absoluteFaceSize, absoluteFaceSize), new Size());
        }

        return faces.toArray();
    }

    /**
     * Uses the HarrCascade classifier to identify positions of eyes in an image
     * Will be more accurate if the given image has only a face in it.
     * (Apply {@link OpenCVFaceDetector#getFaces(Mat)} first)
     *
     * @param face
     * @return
     */
    private Rect[] getEyes(Mat face) {

        MatOfRect eyes = new MatOfRect();
        if (eyeClassifier != null) {
            eyeClassifier.detectMultiScale(face, eyes);
        }

        return eyes.toArray();
    }

    /**
     * converts openCV {@link Rect} to android {@link RectF}
     *
     * @param rect
     * @return A {@link RectF} equivalent of openCV rect
     */
    private RectF getRectF(Rect rect) {
        RectF rectF = null;
        if (rect != null) {
            rectF = new RectF(rect.x, rect.y, rect.x + rect.width, rect.height + rect.y);
        }
        return rectF;
    }

    /**
     * converts openCV {@link Rect} to android {@link RectF} with an offset
     *
     * @param rect
     * @param offsetX
     * @param offsetY
     * @return A {@link RectF} equivalent of openCV rect offset by the given values
     */
    private RectF getRectF(Rect rect, int offsetX, int offsetY) {
        RectF rectF = null;
        if (rect != null) {
            rectF = new RectF(
                    rect.x + offsetX,
                    rect.y + offsetY,
                    rect.x + rect.width + offsetX,
                    rect.height + rect.y + offsetY);
        }
        return rectF;
    }

}

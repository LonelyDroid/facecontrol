package com.training.yml.facecontrol.view.custom_layouts;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class CustomRecyclerView extends RecyclerView {
    private static final String TAG = CustomRecyclerView.class.getName();

    public CustomRecyclerView(@NonNull Context context) {
        super(context);
    }

    public CustomRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Intercepts touch events and passes it on to its children.
     * Window boundaries are calculated to determine which child to notify
     *
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        super.dispatchTouchEvent(ev);

        if (ev.getAction() == MotionEvent.ACTION_UP) {
            Log.i(TAG, "dispatchTouchEvent: " + ev);
            Log.i(TAG, "dispatchTouchEvent: children" + getChildCount());
            for (int i = getChildCount() - 1; i >= 0; i--) {
                View child = getChildAt(i);
                int[] coord = new int[2];

                child.getLocationInWindow(coord);
                Rect childBound = new Rect(coord[0], coord[1], coord[0] + child.getWidth(), coord[1] + child.getHeight());
                if (childBound.contains((int) ev.getX(), (int) ev.getY())) {
                    if (child.hasOnClickListeners()) {
                        child.performClick();
                    }
                }
            }
        }
        return true;
    }
}

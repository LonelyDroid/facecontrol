package com.training.yml.facecontrol.view.overlays;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class CameraPointerView extends View implements PointerView {
    private static final String TAG = CameraPointerView.class.getName();

    private boolean shouldClear = false;

    private final Paint paint;
    private final List<RectF> rectsToDraw;
    private final List<Pair<Integer, Integer>> pointsToDraw;
    private int[] scalePoint = new int[2];

    public CameraPointerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.GREEN);
        rectsToDraw = new ArrayList<>();
        pointsToDraw = new ArrayList<>();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (RectF r : rectsToDraw) {

            paint.setStyle(Paint.Style.FILL);
            canvas.drawCircle(r.centerX(), r.centerY(), 10, paint);

            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(10);
            canvas.drawCircle(r.centerX(), r.centerY(), r.width() / 2, paint);
        }

        for (Pair<Integer, Integer> p : pointsToDraw) {

            canvas.drawCircle(p.first, p.second, 10, paint);
            Log.i(TAG, "onDraw: point : " + p.toString());
        }

        if (shouldClear) {
            shouldClear = false;
            rectsToDraw.clear();
            pointsToDraw.clear();
        }
    }

    @Override
    public void addPoint(Pair<Integer, Integer> point) {
        pointsToDraw.add(point);
    }

    @Override
    public void addRect(RectF rectangle) {
        rectsToDraw.add(rectangle);
    }

    @Override
    public void clear() {
        shouldClear = true;
    }

    @Override
    public void reDraw() {
        invalidate();
    }

    @Override
    public void setColor(int color) {
        paint.setColor(color);
    }
}

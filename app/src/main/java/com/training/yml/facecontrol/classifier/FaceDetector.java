package com.training.yml.facecontrol.classifier;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.Log;
import android.util.Pair;

import com.training.yml.facecontrol.view.ControlView;
import com.training.yml.facecontrol.view.overlays.PointerView;

/**
 * Base class for using a face controlled system.
 * Handles cursor location, scrolling and click events.
 */
public abstract class FaceDetector {
    private static final String TAG = FaceDetector.class.getName();

    /**
     * Number of times a gaze is detected unchanged before dispatching a click event
     */
    private final int CLICK_DELAY = 9;

    /**
     * Margin to consider as the edge of a view to trigger scrolling
     */
    private final int SCROLL_MARGIN = 100;

    /**
     * The speed at which a view is scrolled
     */
    private final int SCROLL_SPEED = 50;


    protected float scaleFactorX = 3;
    protected float scaleFactorY = 3;
    protected int cursorBoundSize = 40;

    protected PointerView pointerView;
    protected ControlView controlView;

    protected int[] trueFaceCenter = new int[2];
    protected int[] scaleCenter = new int[2];
    protected int[] cursorCenter = new int[2];
    protected Rect cursorRect = new Rect(0, 0, 0, 0);
    private int stationaryCount;

    public FaceDetector(ControlView controlView) {
        this.controlView = controlView;
        scaleCenter[0] = 500;
        scaleCenter[1] = 1000;
    }

    public void setPointerView(PointerView pointerView) {
        this.pointerView = pointerView;
    }

    /**
     * The actual face movement is scaled over a larger display.
     *
     * @param x actual x co-ordinate on display
     * @param y actual y co-ordinate on display
     * @return pair of scaled co-ordinates
     */
    protected Pair<Integer, Integer> scale(float x, float y) {
        cursorCenter[0] = (int) (scaleFactorX * (x - scaleCenter[0]) + scaleCenter[0]);
        cursorCenter[1] = (int) (scaleFactorY * (y - scaleCenter[1]) + scaleCenter[1]);
        updateCursorRect();
        return new Pair<>(cursorCenter[0], cursorCenter[1]);
    }

    /**
     * Responsible for updating the cursor position.
     * Takes current scaled cursor and compares it to the current cursor.
     * If its within {@link FaceDetector#cursorBoundSize} it discards the update.
     * else updates the cursor.
     * Additionally checks for applicable cursor actions like scroll and tap.
     */
    protected void updateCursorRect() {

        Rect newCursor = new Rect(
                cursorCenter[0] - cursorBoundSize,
                cursorCenter[1] - cursorBoundSize,
                cursorCenter[0] + cursorBoundSize,
                cursorCenter[1] + cursorBoundSize);

        if (!newCursor.intersect(cursorRect)) {
            cursorRect = newCursor;
            stationaryCount = 0;
        } else {
            checkForAction();
            stationaryCount++;
        }
    }

    private void checkForAction() {
        if (cursorRect.centerY() < SCROLL_MARGIN) {
            controlView.scroll(0, -SCROLL_SPEED);
        } else if (cursorRect.centerY() > (controlView.getDimen().bottom - SCROLL_MARGIN)) {
            controlView.scroll(0, SCROLL_SPEED);
        } else if (cursorRect.centerX() < SCROLL_MARGIN) {
            controlView.scroll(-SCROLL_SPEED, 0);
        } else if (cursorRect.centerX() > (controlView.getDimen().right - SCROLL_MARGIN)) {
            controlView.scroll(SCROLL_SPEED, 0);
        } else if (stationaryCount == CLICK_DELAY) {
            if (controlView != null) {
                Log.i(TAG, "updateCursorRect: clicked " + cursorRect);
                pointerView.setColor(Color.BLUE);
                controlView.tap(cursorRect.centerX(), cursorRect.centerY());
            }
        } else if ((stationaryCount / 3) <= 1) {
            pointerView.setColor(Color.parseColor("#2196F3"));
        } else if ((stationaryCount / 3) <= 2) {
            pointerView.setColor(Color.RED);
        }
    }

    public Rect getCursorRect() {
        return cursorRect;
    }

    public void setScaleFactorX(float scaleFactorX) {
        this.scaleFactorX = scaleFactorX;
    }

    public void setScaleFactorY(float scaleFactorY) {
        this.scaleFactorY = scaleFactorY;
    }

    /**
     * calibrates the display to match the face center.
     */
    public void calibrateCenter() {
        scaleCenter = trueFaceCenter.clone();
    }

    /**
     * processes a bitmap image and determines the gaze location
     *
     * @param image
     * @param detectLandmarks additionally detects facial landmarks such as eyes
     */
    abstract public void processBitmap(Bitmap image, boolean detectLandmarks);
}

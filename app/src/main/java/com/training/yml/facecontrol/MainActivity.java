package com.training.yml.facecontrol;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;

import com.google.firebase.FirebaseApp;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.training.yml.facecontrol.adapters.TextAdapter;
import com.training.yml.facecontrol.classifier.FaceDetector;
import com.training.yml.facecontrol.classifier.MLKitFaceDetector;
import com.training.yml.facecontrol.classifier.OpenCVFaceDetector;
import com.training.yml.facecontrol.view.ControlView;
import com.training.yml.facecontrol.view.overlays.CameraPointerView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements ControlView {

    private static final String TAG = MainActivity.class.getName();
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 1;
    public static final String CAMERA_THREAD_NAME = "camera_background_thread";

    private TextureView cameraViewMain;
    private CameraPointerView cameraPointerView;
    private ConstraintLayout rootView;
    private RecyclerView listVertical;
    private RecyclerView listHorizontal;

    private CameraManager cameraManager;
    private int cameraFacing;
    private TextureView.SurfaceTextureListener surfaceTextureListener;
    private android.util.Size previewSize;
    private String cameraId;
    private CameraDevice cameraDevice;
    private HandlerThread backgroundThread;
    private Handler backgroundHandler;
    private CaptureRequest.Builder captureRequestBuilder;
    private CaptureRequest captureRequest;
    private CameraCaptureSession cameraCaptureSession;
    private SettingsSharedPref settings;

    private FaceDetector faceDetector;


    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    Log.i(TAG, "OpenCV loaded successfully");
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    private CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice cameraDevice) {
            MainActivity.this.cameraDevice = cameraDevice;
            createPreviewSession();
        }

        @Override
        public void onDisconnected(CameraDevice cameraDevice) {
            cameraDevice.close();
            MainActivity.this.cameraDevice = null;
        }

        @Override
        public void onError(CameraDevice cameraDevice, int error) {
            cameraDevice.close();
            MainActivity.this.cameraDevice = null;
        }
    };
    private FirebaseVisionFaceDetectorOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        FirebaseApp.initializeApp(getApplicationContext());
        settings = new SettingsSharedPref(this);

        cameraViewMain = findViewById(R.id.txv_camera_main);
        cameraPointerView = findViewById(R.id.pv_indicator_main);
        rootView = findViewById(R.id.cl_root_mainView);
        listVertical = findViewById(R.id.rv_list_vertical);
        listHorizontal = findViewById(R.id.rv_list_horizontal);

        List<String> dummyStrings = new ArrayList<>();
        for (int i = 0; i < 40; i++) {
            dummyStrings.add("Category " + i);
        }
        TextAdapter adapter = new TextAdapter(dummyStrings, R.layout.text_item_layout);
        listHorizontal.setAdapter(adapter);
        listHorizontal.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        List<String> largeDummyStrings = new ArrayList<>();
        for (int i = 0; i < 40; i++) {
            largeDummyStrings.add(getString(R.string.lorem_ipsum) + i);
        }
        TextAdapter largeTextAdapter = new TextAdapter(largeDummyStrings, R.layout.large_text_item_layout);
        listVertical.setAdapter(largeTextAdapter);
        listVertical.setLayoutManager(new LinearLayoutManager(this));

        requestCameraPermission();
        cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        cameraFacing = CameraCharacteristics.LENS_FACING_FRONT;

        surfaceTextureListener = new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
                setUpCamera();
                openCamera();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
                faceDetector.processBitmap(cameraViewMain.getBitmap(), false);
            }
        };

        FloatingActionButton fabCalibrate = findViewById(R.id.fab_calibrate);
        fabCalibrate.setOnClickListener(view -> faceDetector.calibrateCenter());

        FloatingActionButton fabSettings = findViewById(R.id.fab_settings);
        fabSettings.setOnClickListener(view -> showSettings());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);

            final int classifierSet = settings.getClassifier();
            if (classifierSet == SettingsSharedPref.OPEN_CV) {
                CascadeClassifier faceModel = getCascadeClassifier(R.raw.haarcascade_frontalface);
                CascadeClassifier eyeModel = getCascadeClassifier(R.raw.haarcascade_eye);
                faceDetector = new OpenCVFaceDetector(faceModel, eyeModel, this);
            } else if (classifierSet == SettingsSharedPref.FML_KIT) {
                faceDetector = new MLKitFaceDetector(this);
            }

            faceDetector.setScaleFactorX(settings.getXSensitivity());
            faceDetector.setScaleFactorY(settings.getYSensitivity());
            faceDetector.setPointerView(cameraPointerView);

            final int horizontalVisibility = settings.getShowHorizontalList() ? View.VISIBLE : View.GONE;
            final int verticalVisibility = settings.getShowVerticalList() ? View.VISIBLE : View.GONE;

            listHorizontal.setVisibility(horizontalVisibility);
            listVertical.setVisibility(verticalVisibility);

            openBackgroundThread();

            if (cameraViewMain.isAvailable()) {
                setUpCamera();
                openCamera();
            } else {
                cameraViewMain.setSurfaceTextureListener(surfaceTextureListener);
            }
        }

    }

    private void openBackgroundThread() {
        backgroundThread = new HandlerThread(CAMERA_THREAD_NAME);
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
    }

    private void openCamera() {
        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                cameraManager.openCamera(cameraId, stateCallback, backgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setUpCamera() {
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics cameraCharacteristics =
                        cameraManager.getCameraCharacteristics(cameraId);
                if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == cameraFacing) {
                    StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.get(
                            CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                    previewSize = streamConfigurationMap.getOutputSizes(SurfaceTexture.class)[0];
                    this.cameraId = cameraId;
                }
            }
        } catch (CameraAccessException | NullPointerException e) {
            Log.e(TAG, "setUpCamera: Camera setup failed", e);
        }
    }

    @Override
    protected void onStop() {
        closeCamera();
        closeBackgroundThread();
        super.onStop();
    }

    private void closeCamera() {
        if (cameraCaptureSession != null) {
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }

        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
    }

    private void closeBackgroundThread() {
        if (backgroundHandler != null) {
            backgroundThread.quitSafely();
            backgroundThread = null;
            backgroundHandler = null;
        }
    }

    private void createPreviewSession() {
        try {
            SurfaceTexture surfaceTexture = cameraViewMain.getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
            Surface previewSurface = new Surface(surfaceTexture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(previewSurface);

            cameraDevice.createCaptureSession(Collections.singletonList(previewSurface),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                            if (cameraDevice == null) {
                                return;
                            }

                            try {
                                captureRequest = captureRequestBuilder.build();
                                MainActivity.this.cameraCaptureSession = cameraCaptureSession;
                                MainActivity.this.cameraCaptureSession.setRepeatingRequest(captureRequest,
                                        null, backgroundHandler);
                            } catch (CameraAccessException e) {
                                Log.e(TAG, "onConfigured: camera could not be accessed", e);
                            }
                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {

                        }
                    }, backgroundHandler);
        } catch (CameraAccessException e) {
            Log.e(TAG, "onConfigured: camera could not be accessed", e);
        }
    }

    private CascadeClassifier getCascadeClassifier(int xmlResId) {
        CascadeClassifier classifier = null;
        try {
            InputStream is = getResources().openRawResource(xmlResId);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, String.valueOf(UUID.randomUUID()));
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            classifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());

        } catch (Exception e) {
            Log.e(TAG, "Error loading cascade ", e);
        }

        return classifier;
    }

    private void showSettings() {

        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);

    }

    void requestCameraPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        CAMERA_PERMISSION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "onRequestPermissionsResult: camera permission granted");
                } else {
                    Log.i(TAG, "onRequestPermissionsResult: Camera permission denied");
                }

            }
        }
    }

    @Override
    public void scroll(int x, int y) {
        if (x == 0) { // vertical scroll
            Log.i(TAG, "scroll: " + x + ", " + y);
            listVertical.smoothScrollBy(x, y);
        } else if (y == 0) { // horizontal scroll
            listHorizontal.smoothScrollBy(x, y);
        }
    }

    @Override
    public void tap(int x, int y) {

        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis() + 100;
        int metaState = 0;

        MotionEvent motionEvent = MotionEvent.obtain(
                downTime,
                eventTime,
                MotionEvent.ACTION_UP,
                x,
                y,
                metaState
        );

        Log.i(TAG, "tap: dispatching " + x + ", " + y);
        rootView.dispatchTouchEvent(motionEvent);
    }

    @Override
    public Rect getDimen() {
        Rect bound = new Rect();

        int[] coord = new int[2];
        cameraViewMain.getLocationInWindow(coord);
        final int height = cameraViewMain.getHeight();
        final int width = cameraViewMain.getWidth();

        bound.set(coord[0], coord[1], coord[0] + width, coord[1] + height);
        return bound;
    }


}

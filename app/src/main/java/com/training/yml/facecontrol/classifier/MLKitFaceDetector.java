package com.training.yml.facecontrol.classifier;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.util.Pair;

import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark;
import com.training.yml.facecontrol.view.ControlView;

import java.util.List;

public class MLKitFaceDetector extends FaceDetector {
    private static final String TAG = MLKitFaceDetector.class.getName();

    /**
     * this boolean flag is used to drop incoming frames till the current frame is processed
     */
    private volatile boolean processing = false;

    private boolean detectLandmarks;

    private final FirebaseVisionFaceDetector detector;

    public MLKitFaceDetector(ControlView controlView) {
        super(controlView);

        FirebaseVisionFaceDetectorOptions options = new FirebaseVisionFaceDetectorOptions.Builder()
                .setModeType(FirebaseVisionFaceDetectorOptions.ACCURATE_MODE)
                .setLandmarkType(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
                .setClassificationType(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
                .setMinFaceSize(0.5f)
                .setTrackingEnabled(true)
                .build();

        detector = FirebaseVision.getInstance()
                .getVisionFaceDetector(options);

    }

    @Override
    public void processBitmap(Bitmap image, boolean detectLandmarks) {
        this.detectLandmarks = detectLandmarks;
        if (!processing) {
            processing = true;
            FirebaseVisionImage firebaseVisionImage = FirebaseVisionImage.fromBitmap(image);
            detector.detectInImage(firebaseVisionImage)
                        .addOnSuccessListener(this::readResults)
                        .addOnFailureListener(error -> Log.e(TAG, "onFailure: mlkit failed", error));
        }
    }

    private void readResults(List<FirebaseVisionFace> faces) {

        Log.i(TAG, "onSuccess: mlkit got faces " + faces.size());
        processing = false;
        if (faces.size() == 1) {
            final FirebaseVisionFace face = faces.get(0);
            Rect bounds = face.getBoundingBox();
            trueFaceCenter[0] = bounds.centerX();
            trueFaceCenter[1] = bounds.centerY();

            FirebaseVisionFaceLandmark leftEye = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EYE);
            FirebaseVisionFaceLandmark rightEye = face.getLandmark(FirebaseVisionFaceLandmark.RIGHT_EYE);

            if (detectLandmarks) {
                if ((leftEye != null) && (rightEye != null)) {
                    final FirebaseVisionPoint leftEyePoint = leftEye.getPosition();
                    final FirebaseVisionPoint rightEyePoint = rightEye.getPosition();

                    pointerView.addPoint(new Pair<>(leftEyePoint.getX().intValue(), leftEyePoint.getY().intValue()));
                    pointerView.addPoint(new Pair<>(rightEyePoint.getX().intValue(), rightEyePoint.getY().intValue()));
                    pointerView.addRect(new RectF(bounds));
                }
            }

            scale(trueFaceCenter[0], trueFaceCenter[1]);
            pointerView.addRect(new RectF(cursorRect));
            pointerView.reDraw();
            pointerView.clear();

        }
    }
}

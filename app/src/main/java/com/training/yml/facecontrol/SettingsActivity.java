package com.training.yml.facecontrol;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;

public class SettingsActivity extends AppCompatActivity implements
        RadioGroup.OnCheckedChangeListener,
        SeekBar.OnSeekBarChangeListener,
        CompoundButton.OnCheckedChangeListener {
    private static final String TAG = SettingsSharedPref.class.getName();

    private SettingsSharedPref settings;

    private RadioButton rbOpenCV;
    private RadioButton rbFMLK;
    private SeekBar sbXSensitivity;
    private SeekBar sbYSensitivity;
    private CheckBox verticalListToggle;
    private CheckBox horizontalListToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        settings = new SettingsSharedPref(this);

        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        rbOpenCV = findViewById(R.id.rb_openCV);
        rbFMLK = findViewById(R.id.rb_FMLK);
        sbXSensitivity = findViewById(R.id.sb_x_sensitivity);
        sbYSensitivity = findViewById(R.id.sb_y_sensitivity);
        verticalListToggle = findViewById(R.id.cb_verticalList);
        horizontalListToggle = findViewById(R.id.cb_horizontalList);

        final ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setTitle(R.string.settings);
        }

        fillSettings();

        RadioGroup rgClassifiers = findViewById(R.id.rg_classifiers);
        rgClassifiers.setOnCheckedChangeListener(this);

        sbXSensitivity.setOnSeekBarChangeListener(this);
        sbYSensitivity.setOnSeekBarChangeListener(this);

        Button defaultButton = findViewById(R.id.bt_set_defaults);
        defaultButton.setOnClickListener(view -> {
            settings.setDefaults();
            fillSettings();
        });

        verticalListToggle.setOnCheckedChangeListener(this);
        horizontalListToggle.setOnCheckedChangeListener(this);
    }

    private void fillSettings() {
        final int classifier = settings.getClassifier();
        if (classifier == SettingsSharedPref.OPEN_CV) {
            rbOpenCV.setChecked(true);
        } else if (classifier == SettingsSharedPref.FML_KIT) {
            rbFMLK.setChecked(true);
        }

        final int xSensitivity = (int) (settings.getXSensitivity() * 10);
        sbXSensitivity.setProgress(xSensitivity);

        final int ySensitivity = (int) (settings.getYSensitivity() * 10);
        sbYSensitivity.setProgress(ySensitivity);

        verticalListToggle.setChecked(settings.getShowVerticalList());
        horizontalListToggle.setChecked(settings.getShowHorizontalList());
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.rb_openCV) {
            settings.setClassifier(SettingsSharedPref.OPEN_CV);
        } else if (checkedId == R.id.rb_FMLK) {
            settings.setClassifier(SettingsSharedPref.FML_KIT);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (seekBar.getId() == R.id.sb_x_sensitivity) {
            final float sensitivity = seekBar.getProgress() / 10f;
            settings.setXSensitivity(sensitivity);
        } else if (seekBar.getId() == R.id.sb_y_sensitivity) {
            final float sensitivity = seekBar.getProgress() / 10f;
            settings.setYSensitivity(sensitivity);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.cb_horizontalList) {
            settings.setShowHorizontalList(isChecked);
        } else if (buttonView.getId() == R.id.cb_verticalList) {
            settings.setShowVerticalList(isChecked);
        }
    }
}

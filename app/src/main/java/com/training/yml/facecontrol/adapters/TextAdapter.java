package com.training.yml.facecontrol.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.training.yml.facecontrol.R;

import java.util.List;
import java.util.Random;

public class TextAdapter extends RecyclerView.Adapter<TextAdapter.TextListHolder> {

    private List<String> lines;
    private int layoutId;

    public TextAdapter(List<String> lines, int layoutId) {
        this.lines = lines;
        this.layoutId = layoutId;
    }

    public void setLines(List<String> lines) {
        this.lines = lines;
    }

    public static class TextListHolder extends RecyclerView.ViewHolder {

        TextView text;
        CardView card;

        public TextListHolder(@NonNull View itemView) {
            super(itemView);

            text = itemView.findViewById(R.id.tv_content);
            card = itemView.findViewById(R.id.cv_contentCard);
            itemView.setOnClickListener(this::changeColor);
        }

        private void changeColor(View view) {
            Random r = new Random();
            int[] colors = view.getContext().getResources().getIntArray(R.array.md_pallet);
            final int i = r.nextInt(20) % colors.length;
            card.setCardBackgroundColor(colors[i]);
            Toast.makeText(view.getContext(), "Clicked", Toast.LENGTH_SHORT).show();
        }
    }

    @NonNull
    @Override
    public TextListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(layoutId, viewGroup, false);
        return new TextListHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TextListHolder textListHolder, int i) {
        textListHolder.text.setText(lines.get(i));
        int[] colors = textListHolder.card.getContext().getResources().getIntArray(R.array.md_pallet);
        textListHolder.card.setCardBackgroundColor(colors[i % colors.length]);
    }

    @Override
    public int getItemCount() {
        return lines.size();
    }

}

package com.training.yml.facecontrol.view.custom_layouts;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

public class CustomLinearLayout extends LinearLayout {
    private static final String TAG = CustomLinearLayout.class.getName();

    public CustomLinearLayout(Context context) {
        super(context);
    }

    public CustomLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * Intercepts touch events and passes it on to its children.
     * Window boundaries are calculated to determine which child to notify
     *
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        super.dispatchTouchEvent(ev);

        if (ev.getAction() == MotionEvent.ACTION_UP) {
            Log.i(TAG, "dispatchTouchEvent: " + ev);
            for (int i = getChildCount() - 1; i >= 0; i--) {
                View child = getChildAt(i);
                int[] coord = new int[2];

                child.getLocationInWindow(coord);
                Rect childBound = new Rect(coord[0], coord[1], coord[0] + child.getWidth(), coord[1] + child.getHeight());
                if (childBound.contains((int) ev.getX(), (int) ev.getY())) {
                    Log.i(TAG, "dispatchTouchEvent: " + child);
                    if (child.hasOnClickListeners()) {
                        child.performClick();
                    } else {
                        child.dispatchTouchEvent(ev);
                    }
                }
            }
        }
        return true;
    }
}

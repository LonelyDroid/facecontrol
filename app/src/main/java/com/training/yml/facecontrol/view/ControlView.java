package com.training.yml.facecontrol.view;

import android.graphics.Rect;

public interface ControlView {

    /**
     * Dispatches a scroll event to with the given magnitudes
     *
     * @param x magnitude of scroll horizontally (x axis)
     * @param y magnitude of scroll vertically (y axis)
     */
    void scroll(int x, int y);

    /**
     * Dispatches a tap or a click event at the given co-ordinates
     *
     * @param x x co-ordinate on the view
     * @param y y co-ordinate on the view
     */
    void tap(int x, int y);

    /**
     * @return returns a {@link Rect} object with dimensions corresponding to the view
     */
    Rect getDimen();
}
